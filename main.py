#!/usr/bin/env python3
import atexit
import io
import json
import logging
import os
import random
import threading
import time
import traceback
import socket
from queue import Queue
from shutil import copyfile

import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
from insultgenerator import genins

from www import http_server

logging.basicConfig(level=logging.INFO)
os.chdir(os.path.dirname(os.path.abspath(__file__)))


class Config:
    _configKeys = []
    chance: int
    hohlinkas: dict
    token: str
    print_speed: int
    print_time_limit: int
    attachment_chance: int
    video_chance: int
    gif_chance: int
    picture_chance: int
    pictures_dir: str
    picture_text: bool
    http: dict
    _config_file: io.TextIOWrapper

    def __init__(self):
        self.update = threading.Event()

        def open_config_file():
            self._config_file = io.open('config.json', mode='r+', encoding='utf-8')
            atexit.register(lambda: self._config_file.close())
            for key, value in json.load(self._config_file).items():
                setattr(self, key, value)
                self._configKeys.append(key)

        try:
            open_config_file()
        except FileNotFoundError:
            copyfile('config.json.example', 'config.json')
            open_config_file()

        def is_port_in_use(port):
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                return s.connect_ex(('localhost', port)) == 0

        if self.http:
            ws_port = self.http['port']
            while True:
                self.http.update({'ws_port': ws_port + 1})
                if not is_port_in_use(ws_port):
                    break

        if not 1 <= self.chance <= 100:
            raise Exception('Неправильная вероятность сообщения!')

        try:
            with io.open('messages.txt', mode='r', encoding='utf-8') as f:
                self.messages = [i.strip('\n') for i in f.read().split(';') if i.strip('\n')]
        except FileNotFoundError:
            open('messages.txt', 'a').close()
            self.messages = None

        if not self.messages:
            raise Exception('''Почему messages.txt пустой? а? м? Что мне присылать?
            А ну бегом заполнять!''')

        with io.open('no_message.txt', mode='r', encoding='utf-8') as f:
            self.no_message = [i.strip('\n') for i in f.read().split(';') if i.strip('\n')]

        with io.open('videos.txt', mode='r', encoding='utf-8') as f:
            self.videos = f.read().splitlines()

        with io.open('gifs.txt', mode='r', encoding='utf-8') as f:
            self.gifs = f.read().splitlines()

    def write(self):
        config = self.get()
        for client in http_server.ws_clients:
            client.send_message(json.dumps(config))

        self._config_file.seek(0)
        self._config_file.write(json.dumps(config, indent=4))
        self._config_file.truncate()
        self.update.set()

    def get(self) -> dict:
        config = {}
        for key in self._configKeys:
            config.update({key: getattr(self, key)})
        return config


def exception_handler(func):
    def h(*args):
        while True:
            try:
                func(*args)
            except Exception as e:
                traceback.print_tb(e.__traceback__)
                print(e)

    return h


class PrintingQueueHandler(threading.Thread):
    daemon = True

    def __init__(self):
        self.printing_queue = Queue()
        threading.Thread.__init__(self)

    @exception_handler
    def run(self):
        logging.info('Обработчик очереди печатаемых сообщений запущен')
        while True:
            if not self.printing_queue.empty():
                msg, event = self.printing_queue.get()
                if msg == 'attachment':
                    dice = random.randint(1, 100)
                    if dice <= config.picture_chance:
                        bot.send_picture(event)
                    elif dice <= config.picture_chance + config.video_chance:
                        bot.send_video(event)
                    else:
                        bot.send_gif(event)
                else:
                    bot.type_message(msg, event)
            time.sleep(3)


class Bot:
    _used_messages = {}
    _used_pictures = {}
    _used_videos = {}
    _used_gifs = {}

    def __init__(self):
        while not config.token:
            logging.info(f'Не найден токен! Поставьте на странице http://localhost:{config.http["port"]}')
            config.update.wait()
        vk_session = vk_api.VkApi(token=config.token)
        self._vk_upload = vk_api.upload.VkUpload(vk_session)
        self._longpoll = VkLongPoll(vk_session)
        self._vk = vk_session.get_api()

    @classmethod
    def _get_chat_id(cls, event):
        if 'chat_id' in event.__dict__.keys():
            return event.user_id, None, event.chat_id
        else:
            return event.peer_id, event.peer_id, None

    def _random_media(self, variants: list, user_id: int, used: str):
        if user_id in getattr(self, used).keys():
            while True:
                media = random.choice(variants)
                if media not in getattr(self, used)[user_id]:
                    break
            if len(getattr(self, used)[user_id]) == 5:
                getattr(self, used)[user_id].pop(0)
            getattr(self, used)[user_id].append(media)
        else:
            media = random.choice(variants)
            getattr(self, used).update({user_id: [media]})
        return media

    def type_message(self, msg, event):
        user_id, peer_id, chat_id = self._get_chat_id(event)
        print_time = round(len(msg) / config.print_speed)
        print_time = print_time if print_time <= config.print_time_limit else config.print_time_limit
        logging.info(f'Печатаю сообщение: {print_time}sec для {user_id}')
        for i in range(round(print_time / 9)):
            self._vk.messages.setActivity(peer_id=event.peer_id, type='typing')
            time.sleep(9)
        self._vk.messages.send(chat_id=chat_id,
                               peer_id=peer_id,
                               message=msg,
                               reply_to=event.message_id,
                               random_id=0)
        logging.info('Отправлено')

    @classmethod
    def _genins(cls):
        if config.picture_text:
            return genins()
        else:
            return ''

    def send_picture(self, event):
        user_id, peer_id, chat_id = self._get_chat_id(event)
        picture = self._random_media(os.listdir(config.pictures_dir), user_id, '_used_pictures')

        logging.info(f'Отправляю картинку: {picture}')
        photo = self._vk_upload.photo_messages(photos=[os.path.join(config.pictures_dir, picture)])[0]
        self._vk.messages.send(chat_id=chat_id,
                               peer_id=peer_id,
                               message=self._genins(),
                               attachment=f'photo{photo["owner_id"]}_{photo["id"]}_{photo["access_key"]}',
                               reply_to=event.message_id,
                               random_id=0)

    def send_video(self, event):
        user_id, peer_id, chat_id = self._get_chat_id(event)
        video = self._random_media(config.videos, user_id, '_used_videos')

        logging.info(f'Отправляю видео: {video}')
        self._vk.messages.send(chat_id=chat_id,
                               peer_id=peer_id,
                               message=self._genins(),
                               attachment=video,
                               reply_to=event.message_id,
                               random_id=0)

    def send_gif(self, event):
        user_id, peer_id, chat_id = self._get_chat_id(event)
        gif = self._random_media(config.gifs, user_id, '_used_gifs')

        logging.info(f'Отправляю GIF: {gif}')
        self._vk.messages.send(chat_id=chat_id,
                               peer_id=peer_id,
                               message=self._genins(),
                               attachment=gif,
                               reply_to=event.message_id,
                               random_id=0)

    def _domain_to_id(self, domain) -> str:
        """
        Возвращает айди по домену, e.g durov -> "1"
        :param domain: домен, с которым, работаем
        :return: ID пользователя с этим доменом
        """
        data = self._vk.users.get(user_ids=domain)
        try:
            user_id = data[0]['id']
        except KeyError:
            raise ValueError(data)
        return user_id

    @classmethod
    def _id_from_message(cls, message) -> str:
        """
        Извлекает id из сообщения
        :param message: сообщение, из которого извлекаем ID
        :return: id в виде строки
        """
        try:
            user_id = int(message)
        except ValueError:
            raise ValueError("Неверный ID")
        return str(user_id)

    def _admin_functional(self, event):
        """
        Логика добавления в список и исключения из него хохлинок
        :param event: ивент с сообщением, на которое отвечаем
        """
        gay_id: str = ""
        # Извлекаем ID новоприбывшей хохлинки из сообщения
        # Примеры:
        #   $durov
        #   $id1
        #   $123456789
        if event.text.startswith('$'):
            message = event.text.lstrip("$")
            try:
                gay_id = self._id_from_message(message)
            except ValueError:
                try:
                    gay_id = self._domain_to_id(message)
                except ValueError:
                    logging.error("Получена команда для добавления хохлинки, но пользователь в сообщении не был найден")
                    return
            self._vk.messages.delete(message_ids=event.message_id, delete_for_all=1)

        # ID новоприбывшей хохлинки берём из пересланного сообщения
        elif event.text == '!':
            msg = self._vk.messages.getById(message_ids=[event.message_id])['items'][0]
            if event.from_me and event.text == '!':
                if 'reply_message' in msg.keys():
                    gay_id = msg['reply_message']['from_id']
                elif 'fwd_messages' in msg.keys():
                    gay_id = msg['fwd_messages'][0]['from_id']
            self._vk.messages.delete(message_ids=event.message_id, delete_for_all=1)

        # Не нашли айди, можем скипать сообщение
        if not gay_id:
            return

        gay_id = str(gay_id)

        hohol_ids = config.hohlinkas.keys()

        if gay_id in hohol_ids and event.peer_id in config.hohlinkas[gay_id]:
            config.hohlinkas[gay_id].remove(event.peer_id)
            if not config.hohlinkas[gay_id]:
                config.hohlinkas.pop(gay_id)
            logging.info(f'Больше не хохлинка id{gay_id} в чате {event.peer_id}')
        else:
            try:
                config.hohlinkas[gay_id].append(event.peer_id)
            except KeyError:
                config.hohlinkas.update({gay_id: [event.peer_id]})
            logging.info(f'Новая хохлинка - id{gay_id} в чате {event.peer_id}')

        config.write()

    def _trolling(self, event):
        """
        Основная логика троллинга
        :param event: ивент с сообщением, на которое отвечаем
        """

        # Если нет сообщения, с 50% шансом писать сообщение из no_message.txt
        if len(event.text) == 1:
            if random.randint(1, 10) > 5:
                msg = random.choice(config.no_message)
                logging.info(f'В очередь добавлено с. для {event.user_id}')
                printingQueueHandler.printing_queue.put((msg, event))
            return

        dice = random.randint(1, 100)
        if dice <= config.attachment_chance:
            logging.info(f'В очередь добавлено вл. для {event.user_id}')
            printingQueueHandler.printing_queue.put(('attachment', event))
        elif event.user_id not in [i[1].user_id for i in printingQueueHandler.printing_queue.queue]:
            if event.user_id in self._used_messages.keys():
                while True:
                    msg = random.choice(config.messages)
                    if msg not in self._used_messages[event.user_id]:
                        break
                if len(self._used_messages[event.user_id]) == 5:
                    self._used_messages[event.user_id].pop(0)
                self._used_messages[event.user_id].append(msg)
            else:
                msg = random.choice(config.messages)
                self._used_messages.update({event.user_id: [msg]})
            logging.info(f'В очередь добавлено с. для {event.user_id}')
            printingQueueHandler.printing_queue.put((msg, event))

    @classmethod
    def _is_time_to_trolling(cls, event) -> bool:
        """
        Проверка нужно ли отвечать троллингом на это сообщение
        :param event: ивент с сообщением
        :return: True, если нужно ответить
        """
        user_id = str(event.user_id)  # ID пользователя
        peer_id = event.peer_id  # ID чата, из которого пришло сообщение
        spam_into = config.hohlinkas.get(user_id, [])  # Список чатов, в которых надо троллить хохла

        return peer_id in spam_into and random.randint(1, 100) <= config.chance

    @exception_handler
    def start(self):
        logging.info('Бот работает')
        for event in self._longpoll.listen():

            # Если не сообщение, то скипаем
            if event.type != VkEventType.MESSAGE_NEW:
                continue

            # Собственные сообщения
            elif event.from_me:
                self._admin_functional(event)

            # Проверка на хохла
            elif self._is_time_to_trolling(event):
                self._trolling(event)


if __name__ == "__main__":
    printingQueueHandler = PrintingQueueHandler()
    printingQueueHandler.start()
    config = Config()
    if config.http:
        httpd = http_server.WebServer(config.http['host'], config.http['port'], config)
        ws = http_server.WsServer(config.http['host'], config.http['ws_port'])
    bot = Bot()
    bot.start()
