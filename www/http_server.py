import atexit
import http.server
import json
import logging
import os
import re
import socketserver
import threading
import sys
import webbrowser
from urllib.request import urlopen

from simple_websocket_server import WebSocket, WebSocketServer

sys.path.append('...')
from main import Config

ws_clients = []


class WebServer(socketserver.TCPServer):
    class _RequestHandler(http.server.SimpleHTTPRequestHandler):
        config: Config
        ws_port: int

        def __init__(self, *args, **kwargs):
            super().__init__(*args, directory=os.path.join(os.path.curdir, 'www', 'html'), **kwargs)

        def _json_response(self, code):
            self.send_response(code)
            self.send_header('Content-type', 'application/json')
            self.end_headers()

        def _json_echo(self, json_obj, code=200):
            self._json_response(code)
            self.wfile.write(json.dumps(json_obj, ensure_ascii=False).encode('utf-8'))

        def do_GET(self):
            if self.path == '/get/config':
                self._json_echo(self.config.get())
            elif self.path == '/get/images':
                self._json_echo(os.listdir(os.path.join(self.config.pictures_dir, '..')))
            elif self.path.startswith('/http'):
                with urlopen(self.path[1:]) as response:
                    self.send_response(200)
                    for header in response.getheaders():
                        self.send_header(*header)
                    self.end_headers()
                    self.wfile.write(response.read())
            else:
                return http.server.SimpleHTTPRequestHandler.do_GET(self)

        def do_POST(self):
            if self.path == '/set/config':
                try:
                    content_length = int(self.headers['Content-Length'])
                    new_config = json.loads(self.rfile.read(content_length))
                    for key, value in new_config.items():
                        setattr(self.config, key, value)
                    self.config.write()
                    self._json_echo(['ok'])
                except Exception as _:
                    self._json_echo(['error'], 500)

        def log_message(self, f, *args):
            pass

    class _ServerThread(threading.Thread):
        daemon = True

        def __init__(self, httpd, logger):
            self._httpd = httpd
            self._logger = logger
            atexit.register(lambda: self.stop())
            super().__init__()

        def run(self):
            self._httpd.serve_forever()

        def stop(self):
            self._httpd.server_close()
            self._logger.info('Сервер остановлен.')

    def __init__(self, host: str, port: int, config: Config):
        self._RequestHandler.config = config
        super().__init__((host, port), self._RequestHandler)
        logger = logging.getLogger('www')
        self._thread = self._ServerThread(self, logger)
        self._thread.start()
        webbrowser.open(f"http://localhost:{port}")
        logger.info('Сервер запущен.')


class WsServer(WebSocketServer):
    class _WsHandler(WebSocket):
        def connected(self):
            ws_clients.append(self)

        def handle_close(self):
            ws_clients.remove(self)

    class _WsThread(threading.Thread):
        daemon = True

        def __init__(self, ws, logger):
            self.ws = ws
            self._logger = logger
            atexit.register(lambda: self.stop())
            super().__init__()

        def run(self):
            self.ws.serve_forever()

        def stop(self):
            self.ws.close()
            self._logger.info('Сервер остановлен.')

    def __init__(self, host: str, port: int):
        super().__init__(host, port, self._WsHandler)
        logger = logging.getLogger('ws')
        self.thread = self._WsThread(self, logger)
        self.thread.start()
        logger.info('Сервер запущен.')
